---
title: Learning Materials
title_image: mountains-nature-arrow-guide-66100.jpg
layout: services/default
author: none
additional_css:
    - title/service-title-buttons.css
redirect_from:
    - services/overall/tutorials
    - services/overall/guidelines
    - guidelines
    - workshop-materials
excerpt:
  "We provide learning materials for all purposes related to software engineering and cloud services usage."
---

{{ page.excerpt }}

## Workshop Materials

Our workshop material is available for self-guided learning or as a basis for your own workshops.
Its use and reuse is permitted under a Creative Commons License (see the footers on the workshop pages).

If you find issues or would like to contribute, feel free to take a look at the respective repositories.

### Python

| Topic                       | Materials                              | Repository                           |
|-----------------------------|----------------------------------------|--------------------------------------|
| Object-Oriented Programming | [Material][python-oop-material]        | [Repository][python-oop-repo]        |
| The _Pandas_ framework      | [Material][python-pandas-material]     | [Repository][python-pandas-repo]     |
| The _Matplotlib_ framework  | [Material][python-matplotlib-material] | [Repository][python-matplotlib-repo] |

[python-matplotlib-material]: {{ "workshop-materials/python-matplotlib/" | relative_url}}
[python-matplotlib-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/workshop-matplotlib
[python-oop-material]: {{ "workshop-materials/python-oop/" | relative_url }}
[python-oop-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/workshop-oop-in-python
[python-pandas-material]: {{ "workshop-materials/python-pandas/" | relative_url }}
[python-pandas-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/workshop-pandas

### Further Materials

Not all of our workshop materials are published in a nice fashion (yet).
You can find the complete set of materials in the [Helmholtz Codebase](https://gitlab.hzdr.de/hifis/software/education/hifis-workshops).

## Tutorials

<div class="flex-cards">
{%- assign posts = site.categories['Tutorial'] | where: "lang", "en" | sort_natural: "title" -%}
{% for post in posts -%}
{% include cards/post_card.html post=post exclude_date=true %}
{% endfor -%}
</div>

## Guidelines

<div class="flex-cards">
{%- assign posts = site.categories['Guidelines'] | where: "lang", "en" | sort_natural: "title" -%}
{% for post in posts -%}
{% include cards/post_card.html post=post exclude_date=true %}
{% endfor -%}
</div>

<div class="alert alert-success">
  <h2 id="contact-us"><i class="fas fa-info-circle"></i> Comments or Suggestions?</h2>
  <p>
    Feel free to <a href="{% link contact.md %}">contact us</a>.
  </p>
</div>
