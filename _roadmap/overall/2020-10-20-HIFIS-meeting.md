---
date: 2020-10-01
title: Tasks in Oct 2020
service: overall
---

## HIFIS Meeting

A meeting of all HIFIS members is planned to foster collaboration between the HIFIS clusters. 
Non-HIFIS Helmholtz-centres as well as all incubator platforms shall be incorporated.
