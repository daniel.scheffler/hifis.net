---
date: 2022-09-29
title: Tasks in September 2022
service: overall
---

## HIFIS Review
After a bit more than three years of initial phase, the progress and further perspectives of HIFIS will be reviewed.
Earlier in 2022, potential perspectives will be proposed and formulated in a pre-review report.
End of September, the main review meeting will take place at DESY.
