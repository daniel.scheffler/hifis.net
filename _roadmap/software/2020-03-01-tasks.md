---
date: 2020-03-01
title: Tasks in March 2020
service: software
---

## [Start of Helmholtz-wide training events]({% link events.md %})
The first Helmholtz-wide training events are conducted.
Read the [announcement post]({% post_url 2020/02/2020-02-21-HIFIS-workshops-2020 %})
for information on how the initial course portfolio will look like.
