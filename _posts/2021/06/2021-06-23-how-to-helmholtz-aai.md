---
title: "AAI login to Helmholtz Cloud and Associated Services"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
data: 2021-06-23
authors:
  - "Uwe Jandt"
layout: blogpost
categories:
  - Tutorial
redirect_to:
  - https://aai.helmholtz.de/tutorial/2021/06/23/how-to-helmholtz-aai.html
excerpt: >
    Pictured tutorial on usage of Helmholtz AAI login and registration procedures for Helmholtz Cloud and Associated services, using the HIFIS Events Management Platform at <a href="https://events.hifis.net">events.hifis.net</a> as an example.
---
