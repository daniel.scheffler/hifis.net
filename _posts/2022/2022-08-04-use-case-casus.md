---
title: "CASUS uses HIFIS services for data-centric research"
title_image: CASUS-Center-Advanced-System-Understanding.jpg 
data: 2022-08-04
authors:
  - "Bussmann, Michael"
layout: blogpost
categories:
  - Use-Case
tags:
  - Helmholtz AAI
  - OpenStack (HDF Cloud)
  - Container Runtime
  - Ranger managed Kubernetes
  - Jupyter
  - HZDR
excerpt: >
    The HIFIS service infrastructure is essential to the CASUS strategy towards data-centric research of complex systems across scientific communities and research fields.
---

## HIFIS provides software infrastructure for access to large data sets and digital workflows

<figure>
    <img src="{% link assets/img/posts/2022-08-04-use-case-casus/casus-logo.jpg %}" alt="CASUS — Center for Advanced Systems Understanding" style="float:right;width:33%;min-width:20px;padding:5px 5px 20px 20px;">
</figure>

The HIFIS service infrastructure is essential to the [CASUS](https://www.casus.science/) strategy towards data-centric research of complex systems across scientific communities and research fields. 
As an institute of the Helmholtz-Zentrum Dresden-Rossendorf ([HZDR](https://www.hzdr.de)) working on complex systems research using digital technologies such as artificial intelligence, high performance computing or scalable visual and data analytics, we focus on providing cloud-centric solutions for scientific communities and other stakeholders. 
These solutions provide unique services for researchers, companies, governmental entities and international organizations.

While the hardware infrastructure and the domain specific software solutions are provided by CASUS, we rely on the infrastructure of HIFIS for federated authentication, authorization and secure, scalable cloud backends, containerization and orchestration to provide both access to large data sets and the digital workflows for knowledge extraction.

Services we built our solutions on are [OpenStack (HDF Cloud)](https://helmholtz.cloud/services?serviceDetails=openstack%20(hdf%20cloud)-FZJ), [Container Runtime](https://helmholtz.cloud/services?serviceDetails=container-runtime-fzj) based on Singularity, [Rancher managed Kubernetes](https://helmholtz.cloud/services?serviceDetails=rancher-desy), [Jupyter](https://helmholtz.cloud/services?software=Jupyter,JupyterHub) and the [Helmholtz AAI](https://aai.helmholtz.de/).
For development, we use the Gitlab-based [Helmholtz Codebase](https://helmholtz.cloud/services?software=GitLab&serviceDetails=codebase-hzdr) including CI/CD workflows as well as [Mattermost](https://helmholtz.cloud/services?software=Mattermost&serviceDetails=mattermost-hzdr). 
All these services are provided by HIFIS and leveraging the synergies by not having to maintain these services ourselves are key to providing the solutions we develop to large user communities worldwide with high quality and availability.

In particular, HIFIS and Helmholtz are well-trusted entities that have an international reputation for being able to provide high quality infrastructure for science, research and companies. 
This holds especially true for the software infrastructure and services provided by HIFIS, as the Helmholtz / HIFIS brand provides trust in quality, availability and long-term sustainability of these services.

In the next weeks, we will showcase two of these services, one from the research field Health and one from Earth & Environment which we are currently setting up. Further use cases, e.g. from the Research Field Matter, will follow in the future.

## Get in contact
For CASUS: [Michael Bussmann](mailto:m.bussmann@hzdr.de?subject=[HIFIS-use-case])

For HIFIS: [HIFIS Support](mailto:support@hifis.net?subject=[communication])
