---
layout: spotlight

###############################################################################
# Template for Software spotlights
###############################################################################
# Templates starting with a _, e.g. "_template.md" will not be integrated into
# the spotlights.
#
# Optional settings can be left empty.

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------

# The name of the software
name: NEST

# The date when the software was added to the spotlights YYYY-MM-DD
date_added: 2022-08-08

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: nest/nest_logo.png

# One or two sentences describing the software
excerpt: >
    NEST is a simulator for spiking neuronal networks. A well tested and 
    efficient tool, NEST works on your laptop and also on the world’s largest 
    supercomputers to study behaviour of large networks of neurons.

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image:

# Title at the top, inside the title-content-container
title: NEST simulator

# Add at least one keyword
keywords:
    - modeling
    - simulation
    - spiking neural networks

# The Helmholtz research field
hgf_research_field: Information

# At least one responsible centre
# Please use the full and official name of your centre
hgf_centers: Forschungszentrum Juelich

# List of other contributing organisations (optional)
contributing_organisations:
    - name: NEST-initiative
    - name: Norwegian University of Life Sciences (NMBU)

# List of scientific communities
scientific_community:
    - computational neuroscience
    - neurorobotics
    - theoretical neuroscience
    - advanced compute architectures / neuromorphic computing

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: info@nest-initiative.org

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: github
      link_as: https://github.com/nest/nest-simulator
    - type: webpage
      link_as: https://nest-simulator.org
    - type: mailing-list
      link_as: mailto:users@nest-simulator.org

# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license: GPL-2.0-or-later

# Is the software pricey or free? (optional)
costs: free and open

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    - simulation

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    - Desktop
    - HPC

# List of programming languages (optional)
programming_languages:
    - Python
    - C++

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi: 10.5281/zenodo.6368024

# Funding of the software (optional)
funding:
    - shortname: Horizon 2020
    - shortname: ICEI
    - shortname: ACA
    - shortname: ARA-CSD
---


# NEST: A scalable spiking neural network simulator

NEST is used in computational neuroscience to model and study behavior of large networks of neurons. The models describe
single neuron and synapse behavior and their connections. Different mechanisms of plasticity can be used to investigate
artificial learning and help to shed light on the fundamental principles of how the brain works.

NEST offers convenient and efficient commands to define and connect large networks, ranging from algorithmically determined
connections to data-driven connectivity. Create connections between neurons using numerous synapse models from STDP to gap junctions.

## Features

* Extensive model catalog: NEST offers numerous state-of-the art neuron and synapse models. Textbook standards like integrate-and-fire
  and Hodgkin-Huxley type models are available alongside high quality implementations of models published by the neuroscience community.
  We also offer many examples that showcase how to use them!
* Fast-prototyping:  NESTML provides a framework to create models without the use of C++, with a flexible processing toolchain, written in Python.
* Scalable: NEST works on your laptop and also on the world’s largest supercomputers.
* Efficient: NEST makes the best use of your multi-core computer or compute cluster. NEST can seamlessly scale to your needs.
* Well-tested: The simulator is developed and continuously improved by the NEST community. NEST developers are using
  continuous-integration based workflows in order to maintain high code quality standards for correct and reproducible simulations.
* Community-driven: NEST has fostered a large community of experienced developers and amazing users, who actively contribute
  to the project. Our community extends to related projects, like the teaching tool NEST Desktop, cross-simulator languages 
  like PyNN and neural activity analysis tools like Elephant.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/nest/NEST-HeaderGrafik.jpg" alt="Visualization of NEST.">
<span>NEST simulator</span>
</div>
