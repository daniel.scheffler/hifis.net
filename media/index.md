---
title: Media & Materials
title_image: default
layout: default
additional_css:
    - frontpage.css
    - title/service-title-buttons.css
    - media-page-images.css
additional_js: frontpage.js
redirect_from: pr
excerpt:
    HIFIS Media & Materials.
---

<div class="image-block">

<div class="figure-image right">
<figure>
    <img class="help-image"
        alt="person on the right and person on the left talk about starting a project and how HIFIS can help with that"
        src="{{ site.directory.images | relative_url }}/hifisfor/poster-1.svg" />
    <figcaption style="text-align: center;">Lorna Schütte, CC-BY-NC-SA</figcaption>
</figure>
</div>

<div>
<h3>General Publications</h3>
<ul>
    <li><a href="{{ 'publications' | relative_url }}">List of Publications on HIFIS</a></li>
</ul> 
<h3>Media and Material from HIFIS</h3>
<ul style="line-height:200%">
<li>Meet <b>Sam, the Scientist</b>: HIFIS for Scientific Workflows</li>
    <ul>
    <li><a href="{{ site.directory.images | relative_url }}/hifisfor/HIFIS_poster_claim_contact.svg">Poster</a></li>
    <li><a href="{{ 'media/Postcard_A6_claim.svg' | relative_url }}">Postcard / small Poster</a></li>
    <li><a href="{{ site.directory.videos | relative_url }}/video_full.mp4">Video</a></li>
    </ul>
<li>Helmholtz Resonator Podcast, Episode 172 (October 2021): 
<ul><li>
<a href="https://hifis.net/news/2021/10/18/podcast.html">Blogpost</a> & <a href="https://resonator-podcast.de/2021/res172-hifis">direct link </a><i class="fas fa-headphones"></i></li></ul></li>

<li>Overview HIFIS: Digital Services for Helmholtz & Partners (March 2022)</li>
    <ul>
    <li><a href="{{ 'media/HIFIS_overview_2022_03_en.pdf' | relative_url }}">English Version</a></li>
    <li><a href="{{ 'media/HIFIS_overview_2022_03_de.pdf' | relative_url }}">German Version</a></li>
    </ul>
<li><a href="{{ 'media/Helmholz-Incubator-Folder_2022.pdf' | relative_url }}">Helmholtz Incubator Brochure</a></li>
</ul>
</div>

</div>
