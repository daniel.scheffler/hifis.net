---
title: "deRSE19 - Conference for Research Software Engineers in Germany" 
layout: event
organizers:
  - hammitzsch
  - <a href="https://de-rse.org/en/conf2019/contact.html">deRSE community</a>
type: conference
start:
    date:   "2019-06-04"
end:
    date:   "2019-06-06"
location:
    campus: "GFZ, Telegrafenberg"
    room:   "Haus H, A56"
excerpt:
    " Conference for Research Software Engineers in Germany. "
---

Following the success of the first three international Conference of Research Software Engineers in the UK, the first international conference in Germany addressing research software and the people behind it within the German research landscape will be held at the Albert Einstein Science Park in Potsdam on 4-6 June 2019.

More information on the event can be found on the 
[event page](https://de-rse.org/en/conf2019/).
