---
title: "Helmholtz Hacky Hour #26"
layout: event
organizers:
  - belayneh
type:      hacky-hour
start:
  date:   "2021-04-21"
  time:   "14:00"
end:
  time:   "15:00"
location:
  campus: "Online"
  room:   "<a href=https://meet.gwdg.de/b/max-93j-2ef><i class=\"fas fa-external-link-alt\"></i> meet.gwdg.de</a>"
excerpt:  "<strong>Automation in research software development</strong> Automation as one of the keys towards reproducible research."
---
## Automation in research software development
The Hacky Hour is intended to discuss and learn about different topics in the context of research software development. During the session, you will meet Helmholtz researchers from different fields and have the opportunity to present your favorite tools and techniques. If you want to join, share your experience or have any question on the topic, let us and others know about it in [the meta pad](https://pad.gwdg.de/0HczFKgqS_C9L1QGzfpbJA#).

One way to define  automation is as a way to minimize human interventions to avoid manual tasks that often lead to mistakes. Apart from avoiding mistakes, it also highly benefit to save time and let researchers focus on tasks that require more knowledge. In this hacky hour, we discuss and share our experience on topics of automation in software development in research.

We are looking forward to seeing you!
