---
title: <i class="fas fa-question-circle"></i> FAQ — Frequently Asked Questions
title_image: default
layout: default
excerpt:
    Collection of Frequently Asked Question (FAQ) about HIFIS.
---

## General
{:.text-success}

<details class="my-3">
    <summary>
        <strong id="what-is-the-mission-of-hifis"><a href="#what-is-the-mission-of-hifis">What is the mission of HIFIS?</a></strong>
    </summary>
    <div>
        <b>Our Aim</b>
    <p>The top position of Helmholtz research is increasingly based on cross-centre and international cooperation and common access to data treasure and -services.
At the same time, the significance of a sustainable software development for the research process is recognised.</p>

<p><b>HIFIS builds and sustains an excellent IT infrastructure connecting all Helmholtz research fields and centres.</b></p>

<p>This includes the Helmholtz Cloud, where members of the Helmholtz Association of German Research Centres provide selected IT-Services for joint use. HIFIS further supports Research Software Engineering (RSE) with a high level of quality, visibility and sustainability.</p>

<p>The HIFIS clusters develop technologies, processes and policy frameworks for harmonized access to fundamental backbone services, such as Helmholtz AAI, and Helmholtz Cloud services. HIFIS Software also provides education, technology, consulting and community services.</p>

<p><b>Helmholtz Digital Services for Science — Collaboration made easy.</b></p>
        <p><i class="fas fa-headphones"></i>
        <strong>You are invited to <a href="https://resonator-podcast.de/2021/res172-hifis/">tune in to the recent Resonator podcast on HIFIS</a>, to get an idea on our aims and scope!</strong> (German only)
        <i class="fas fa-headphones"></i></p>
    </div>
</details>

<details class="my-3">
    <summary>
        <strong id="how-can-hifis-help-me-as-a-researcher"><a href="#how-can-hifis-help-me-as-a-researcher">How can HIFIS help me as a researcher?</a></strong>
    </summary>
    <div>
        <p>The purpose of HIFIS is to support science and scientists. Within the HIFIS platform, we are building:</p>
        <ul>
        <li>The <a href="https://helmholtz.cloud"><strong>Helmholtz Cloud</strong></a>, providing a portfolio of seamlessly accessible IT services to simplify your daily work.</li>
        <li>All cloud services and software trainings and support are free of charge!</li>
        <li>A <strong>Helmholtz-wide login mechanism</strong>, the <a href="https://aai.helmholtz.de">Helmholtz AAI</a>,</li>
        <li>A stable, high-bandwidth <strong>network infrastructure</strong> connecting all Helmholtz centers,</li>
        <li><a href="{{ "services/software-overview" | relative_url }}"><strong>Software services</strong></a> to provide you with a common platform,
        <a href="{% link events.md %}">training</a> and
        <a href="{% link services/software/consulting.html %}">support</a> for high-quality sustainable software development.</li>
        </ul>
    </div>
</details>

<details class="my-3">
    <summary>
        <strong id="how-can-i-access-cloud-services"><a href="#how-can-i-access-cloud-services">How can I access Cloud Services?</a></strong>
    </summary>
    <div>
        <p>Find all our Cloud Services, as well as any usage details and conditions at the
        <a href="https://helmholtz.cloud/services"><strong>Helmholtz Cloud Portal</strong></a>.</p>
        <p>It is easier than ever before, since you do not need to create new accounts with new passwords!
        Just search for “Helmholtz AAI” when logging in and use your home institute’s credentials.</p>
        <p>Please
        <a href="https://aai.helmholtz.de/tutorial/2021/06/23/how-to-helmholtz-aai.html"><strong>refer to our illustrated tutorial on how to access Cloud Services via Helmholtz AAI</strong></a>
        for more details.</p>
    </div>
</details>

<details class="my-3">
    <summary>
        <strong id="timeline"><a href="#timeline">What is the timeline for HIFIS?</a></strong>
    </summary>
    <div>
        <p>The HIFIS platform was set up in 2019, with basic infrastructure services (AAI) and pilot services online since 2020. The <a href="{% post_url 2020/10/2020-10-13-initial-service-portfolio %}">initial cloud service portfolio has been made public</a> by
        end of 2020;
        the production version of the <a href="{% post_url 2021/11/2021-11-17-CloudPortal %}">Helmholtz Cloud Portal was made available</a> by November 2021.
        The finalisation of the cloud rulebook and GDPR compliant agreement is planned during 2022.</p><p>
        Please also refer to our <a href="{{ "roadmap" | relative_url }}"><strong>HIFIS Roadmap</strong></a> to stay up to date.
        </p>
    </div>
</details>

<details class="my-3">
    <summary>
        <strong id="where-can-i-register-for-a-newsletter"><a href="#where-can-i-register-for-a-newsletter">Where can I register for a newsletter?</a></strong>
    </summary>
    <div>
        <p>Since July 2022, HIFIS is sending quarterly newsletters via <code class="language-plaintext highlighter-rouge">newsletter@hifis.net</code>. You are invited to register to never miss any news and upcoming events! Newsletters published so far can be <a href="{% link newsletter/index.md %}">found here</a>.</p>
        <p>Either <a href="mailto:sympa@desy.de?subject=sub%20hifis-newsletter">register by clicking on this link and sending a registration mail</a> or
        <a href="https://lists.desy.de/sympa/subscribe/hifis-newsletter">register here—you can also unregister here</a>.</p>
        <p>To stay updated on general HIFIS news, you are invited to check out
        <a href="{{ site.feed.collections.posts.path | relative_url }}">our RSS news feed</a>—besides, of course, regularly checking our website and <a href="news">news section!</a></p>
    </div>
</details>

<details class="my-3">
    <summary>
        <strong id="my-helmholtz-center-is-not-directly-involved-into-hifis-do-you-still-help-me"><a href="#my-helmholtz-center-is-not-directly-involved-into-hifis-do-you-still-help-me">My Helmholtz center is not directly involved into HIFIS. Do you still help me?</a></strong>
    </summary>
    <div>
        <p>Yes, of course. HIFIS is a Helmholtz-wide platform that aims to
        provide offers for <strong>all</strong> Helmholtz centers.
        Please <a href="mailto:support@hifis.net">contact us</a> or any of the <a href="{% link team.md %}">HIFIS team members</a> to get assistance.</p>
    </div>
</details>

<details class="my-3">
    <summary>
        <strong id="how-should-i-acknowledge-hifis-assistance-in-a-publication"><a href="#how-should-i-acknowledge-hifis-assistance-in-a-publication">How should I acknowledge HIFIS assistance in a publication?</a></strong>
    </summary>
    <div>
        <p>Please include the following text:</p>
        <blockquote>
        <p>We gratefully acknowledge the HIFIS (Helmholtz Federated IT Services)
        team for … (e.g. support with the <a href="#how-can-hifis-help-me-as-a-researcher">components above</a>)</p>
        </blockquote>
    </div>
</details>




